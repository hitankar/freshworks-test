<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Http\Requests\SurveyRequest;

class SurveyController extends Controller
{
    /**
     * loads the survey view.
     */
    public function index()
    {
        return view('survey');
    }

    /**
     * Save posted data to database.
     */
    public function save(SurveyRequest $request)
    {
        $survey = new \App\Survey();
        $survey->food_type = $request->get('food_type');
        $survey->food_amount = $request->get('food_amount');
        $survey->duck_count = $request->get('duck_count');
        $survey->feeding_location = $request->get('feeding_location');
        $survey->feeding_time = $request->get('feeding_time');
        $survey->save();

        if ($request->get('daily_survey') == 1) {
            $this->scheduleDaily($request);
        }

        if ($survey->id) {
            return response()->json([
                'message' => 'Survey entry added successfully',
                'status' => 'success',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Error encountered: Survey failed to be saved',
                'status' => 'fail',
            ], 500);
        }
    }

    /**
     * Save Daily surveys to separate table.
     */
    private function scheduleDaily(SurveyRequest $request)
    {
        $survey = new \App\DailySurvey();
        $survey->food_type = $request->get('food_type');
        $survey->food_amount = $request->get('food_amount');
        $survey->duck_count = $request->get('duck_count');
        $survey->feeding_location = $request->get('feeding_location');
        $survey->feeding_time = $request->get('feeding_time');
        $survey->save();
    }

    /**
     * Insert daily serveys though cron.
     */
    public function insertDailySurveys()
    {
        $daily_surveys = \App\DailySurvey::get()->toArray();

        foreach ($daily_surveys as $daily_survey) {
            unset($daily_survey['id']);
            $daily_survey['feeding_date'] = Carbon::now()->format('Y-m-d');
            \App\Survey::create($daily_survey);
        }
    }
}
