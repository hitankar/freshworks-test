<?php

namespace App\Http\Controllers;

use App\Survey;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ReportController extends Controller
{
    /**
     * Loads up the reports view.
     */
    public function index()
    {
        return view('reports');
    }

    /**
     * export data from survey table to csv.
     */
    public function export()
    {
        //headers for export
        $headers = [
            'Content-type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename=surveys.csv',
            'Pragma' => 'no-cache',
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Expires' => '0',
        ];

        $surveys = Survey::all();
        $columns = ['Food Type', 'Food Amount', 'Duck Count', 'Feeding Location', 'Feeding Time', 'Feeding Date'];

        // function to iterate through surveys and insert as row to file
        $callback = function () use ($surveys, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($surveys as $survey) {
                fputcsv($file, [$survey->food_type, $survey->food_amount, $survey->duck_count, $survey->feeding_location, $survey->feeding_time, $survey->feeding_date]);
            }
            fclose($file);
        };

        return response()->streamDownload($callback, 'surveys.csv', $headers);
    }
}
