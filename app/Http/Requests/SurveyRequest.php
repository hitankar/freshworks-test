<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SurveyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'food_type' => 'required|max:200',
            'food_amount' => 'required|integer',
            'duck_count' => 'required|integer',
            'feeding_location' => 'required|max:1000',
            'feeding_time' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'foot-type.required' => 'Type of food is required',
            'food-amount.required' => 'Food amount is required',
            'duck-count.required' => 'Duck count is required',
            'feeding-location.required' => 'Feeding location is required',
            'feeding-time.required' => 'Feeding time is required',
        ];
    }
}
