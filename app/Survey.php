<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'surveys';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'feeding_date',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['food_type', 'food_amount', 'duck_count', 'feeding_location', 'feeding_time', 'feeding_date'];

    /**
     * constructor.
     */
    public function __construct(array $attributes = [])
    {
        $this->setRawAttributes([
        'feeding_date' => Carbon::now()->format('Y-m-d'),
        ], true);
        parent::__construct($attributes);
    }
}
