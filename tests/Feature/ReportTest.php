<?php

namespace Tests\Feature;

use Tests\TestCase;
use League\Csv\Reader as CsvReader;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReportTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     *
     * @return void
     */
    public function testReportExport()
    {
        $surveys = factory('App\Survey', 5)->create();
        $response = $this->get('/export-report');

        $response->assertStatus(200);

        $response->assertHeader('content-type', 'text/csv; charset=UTF-8');
        $response->assertHeader('content-disposition', 'attachment; filename=surveys.csv');

        $reader = CsvReader::createFromString($response->streamedContent());
        $reader->setHeaderOffset(0);

        $this->assertCount(\App\Survey::count(), $reader);
    }
}
