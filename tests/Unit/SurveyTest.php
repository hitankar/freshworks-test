<?php

namespace Tests\Unit;

use App;
use Mockery;
use Carbon\Carbon;
use Tests\TestCase;
use App\Http\Controllers;
use Illuminate\Routing\Redirector;
use App\Http\Requests\SurveyRequest;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class SurveyTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    protected $controller;
    protected $data;
    protected $dbFields;

    protected function setUpData()
    {
    }

    /**
     * @test
     */
    public function testSaveSurvey()
    {
        $controller = App::make('App\\Http\\Controllers\\SurveyController');
        $data = [
            'food_type' => $this->faker->word,
            'food_amount' => $this->faker->numberBetween(10, 1000),
            'duck_count' => $this->faker->numberBetween(100, 10000),
            'feeding_location' => $this->faker->address,
            'feeding_time' => $this->faker->time('H:i:s'),
        ];

        $request = SurveyRequest::create('/api/survey', 'POST', $data);

        $request->setContainer(app())
                ->setRedirector(app(Redirector::class))
                ->validateResolved();

        $controller->save($request);

        $this->assertDatabaseHas('surveys', $data);
    }

    /**
     * @test
     */
    public function testInsertDailySurveys()
    {
        $original = App\Survey::count();
        $controller = App::make('App\\Http\\Controllers\\SurveyController');

        factory('App\DailySurvey', 5)->create();

        $controller->insertDailySurveys();

        $new = App\Survey::count();

        $this->assertEquals($new - $original, 5);
    }
}
