/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import 'jquery';
import './autoload/bootstrap';


if(document.getElementById('downloadReport')) {
    const button = document.getElementById('downloadReport');
    button.addEventListener('click', function (event) {
        event.preventDefault();
        const link = event.target.dataset.link;
        window.location.href = link;
    });
}
