import React, { Component } from 'react';
import TextControl from '../components/textcontrol';
import Checkbox from '../components/checkbox';

// container for survey form
class SurveyForm extends Component {
    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
        
        this.state = {
            status: null,
            message: null,
            error: null,
            button_disabled: false,
        }
    }

    // form submit
    handleSubmit = (event) => {
        
        event.preventDefault();

        // get form data
        const data = new FormData(event.target);
        
        // disabling to stop multiclicks
        this.setState({
            button_disabled: true,
        });

        // posting data
        fetch(event.target.action, {
            method: 'POST',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json'
            },
            body: data,
        }).then((response) => response.json())
        .then((json) => {
            
            // if json has status key
            if(json.status) {
                
                // setting output
                this.setState({
                    status: json.status,
                    message: json.message,
                    button_disabled: false
                });
            }
        }).catch((err) => {

            // show error message
            this.setState({
                status: 'danger',
                message: err.message,
                error: err.error,
                button_disabled: false
            });
        });
    }

    // new entry button click event
    handleClick = (event) => {
        this.setState({
            status: null,
            message: null,
            button_disabled: false
        });
    }

    render() {

        const message =
        <div className={"alert alert-" + this.state.status} role="alert">
            {this.state.message}
            <hr />
            <button className="btn btn-primary" onClick={this.handleClick} >New Survey</button>
        </div>

        const form = 
        <form id="surveyForm" method="POST" action="/api/survey" onSubmit={this.handleSubmit}>
                
            <TextControl type="time" id="feedingTime" name="feeding_time" label="Time" />
            <TextControl type="text" id="foodType" name="food_type" label="Type of food fed" />
            <TextControl type="number" id="foodAmount" name="food_amount" label="Amount of food fed (grams)" />
            <TextControl type="text" id="location" name="feeding_location" label="Location" />
            <TextControl type="number" id="duckCount" name="duck_count" label="Number of ducks fed" />
            <Checkbox value="1" name="daily_survey" id="dailySurvey" label="I do this daily" />
            
            <div className="text-center mt-3">
                <button type="submit" className="btn btn-primary w-50" disabled={this.state.button_disabled} >Submit</button>
            </div>
        </form>;

        return (
            <React.Fragment>
            <h2 className="text-center">Survey</h2>
            
            {(this.state.status == null)?form:message}

            </React.Fragment>
        )
    }
}

export default SurveyForm;