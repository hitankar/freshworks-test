import React from 'react';

// functional componenet for text fields
const Checkbox = ({id, label, name, value}) => {

    return (
        <div className="form-group row">
            <label className="col-sm-3 col-form-label">&nbsp;</label>
            <div className="col-sm-9">
                <div className="form-check">
                    <input className="form-check-input" type="checkbox" id={id} value={value} name={name}  />
                    <label className="form-check-label" htmlFor="dailySurvey">
                        {label}
                    </label>
                </div>
            </div>
        </div>
    )
}

export default Checkbox;