import React from 'react';

// functional componenet for text fields
const TextControl = ({type, id, label, name, value}) => {

    return (
        <div className="form-group row">
            <label htmlFor={id} className="col-sm-3 col-form-label">{label}</label>
            <div className="col-sm-9">
                <input type={type} className="form-control" id={id} value={value} name={name} placeholder={label} required />
            </div>
        </div>
    )
}

export default TextControl;