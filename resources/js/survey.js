import React from "react";
import { render } from "react-dom";
import SurveyForm from './containers/surveyform';

const SurveyApp = () => {
    return (
       <SurveyForm />
    )
};

render(<SurveyApp />, document.getElementById("surveyApp"));