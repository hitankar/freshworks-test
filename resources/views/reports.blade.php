@extends('layouts.app')

@section('title', 'Reports')

@section('content')
<div class="container">
    <div class="justify-content-center text-center pt-3 pb-3">
        <h2>Reports</h2>
        <button id="downloadReport" class="btn btn-success" data-link="{{ route('report-export') }}" >Download (CSV Format)</button>
    </div>
</div>
@endsection