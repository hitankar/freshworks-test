<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Freshworks Test</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="/">Survey</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('reports') }}">Reports</a>
      </li>
    </ul>
  </div>
  @if (\Auth::check())
  <form class="form-inline my-2 my-lg-0" method="POST" onClick="javascript:confirm('Are you sure to logout?')" action="{{ route('logout') }}">
      @csrf
      <button class="btn btn-outline-warning my-2 my-sm-0" type="submit">Logout</button>
  </form>
  @endif
</nav>