<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="x-csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- block bots next line -->
    <meta name="robots" content="noindex, nofollow" />

    <title>@yield('title')</title>
    
    <!-- Bootstrap core CSS -->
    <link href="/css/app.css" rel="stylesheet">
    @yield('headscript')

  </head>
  <body class="common">
      @include('partials.header')
      <main role="main" class="container p-0 bg-white">
          @if(Session::has('message'))
          <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
          @endif
        <div class="content mt-5 mb-5" >
            @yield('content')
        </div>
        
      </main><!-- /.container -->
    

      <!-- Bootstrap core JavaScript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      @yield('script')
      <script src="/js/app.js"></script>
  </body>
</html>
