@extends('layouts.app')

@section('title', 'Survey')

@section('content')
<div class="container">
    <div class="justify-content-center pt-3 pb-3">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div id="surveyApp" class="mt-2"></div>
    </div>
</div>
@endsection

@section('script')
    <script src="/js/survey.js"></script>
@endsection