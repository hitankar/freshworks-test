@extends('layouts.app')

@section('title', 'Reports')

@section('content')
<div class="container">
    <div class="justify-content-center text-left pt-3 pb-3">
        <div class="alert alert-info">You have successfully logged out</div>
    </div>
</div>
@endsection