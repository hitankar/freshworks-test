<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Survey;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Survey::class, function (Faker $faker) {
    return [
        'food_type' => $faker->word,
        'food_amount' => $faker->numberBetween(10, 1000),
        'duck_count' => $faker->numberBetween(100, 10000),
        'feeding_location' => $faker->address,
        'feeding_time' => $faker->time('H:i:s'),
        'feeding_date' => Carbon::now()->format('Y-m-d'),
    ];
});
