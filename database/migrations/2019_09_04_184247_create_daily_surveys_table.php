<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailySurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_surveys', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('food_type', 100);
            $table->integer('food_amount')->unsigned();
            $table->integer('duck_count')->unsigned();
            $table->string('feeding_location', 255);
            $table->time('feeding_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_surveys');
    }
}
